ARMv7  Arm64 LuaJit  Unity IOS 使用

mac 上执行 build.py 进行编译

ulua 使用Test5 工程编译

将生成的 libulua.a 和该 libluajit.a 放入xcode中即可使用

ulua编译工程

https://git.oschina.net/liyonghelpme/UnityLuaPerfTest.git

README for LuaJIT 2.1.0-beta2
-----------------------------

LuaJIT is a Just-In-Time (JIT) compiler for the Lua programming language.

Project Homepage: http://luajit.org/

LuaJIT is Copyright (C) 2005-2016 Mike Pall.
LuaJIT is free software, released under the MIT license.
See full Copyright Notice in the COPYRIGHT file or in luajit.h.

Documentation for LuaJIT is available in HTML format.
Please point your favorite browser to:

 doc/luajit.html

