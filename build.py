import os


pwd = os.getcwd()

DEST= os.path.join(pwd, "output")
os.environ["DEST"] = DEST

#DEVELOPER = os.popen("xcode-select -print-path").read().strip()
#os.environ["DEVELOPER"] = DEVELOPER

#IXCODE = DEVELOPER
#os.environ["IXCODE"] = IXCODE

#ISDK=IXCODE+"/Platforms/iPhoneOS.platform/Developer"
#os.environ["ISDK"] = ISDK

#ISDKVER="iPhoneOS10.0.sdk"
#os.environ["ISDKVER"] = ISDKVER

#ISDKP = ISDK+"/usr/lib/"

#xcrun --sdk iphoneos --find clang
#xcrun --sdk iphonesimulator --find clang
#ISDKP = "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/"
#os.environ["ISDKP"] = ISDKP




#os.environ["CC"] = "clang"

ARCHS= "armv7 arm64 i386 x86_64".split()
#ARCHS = "arm64".split()
#ARCHS = "armv7".split()
#ARCHS = "i386".split()
#ARCHS = "x86_64".split()


LIBS="libluajit.a".split()
os.system("mkdir -p %s" % (DEST))

#ISDKP = os.popen("xcrun --sdk iphoneos --show-sdk-path").read().strip()
#ICC = os.popen("xcrun --sdk iphoneos --find clang").read().strip()


ISDKP = ""
ISDK = ""
ISDKF = ""
cmd = ""
for arch in ARCHS:
	os.system("make clean")

	if arch == "armv7":
		ISDK = os.popen("xcodebuild -version -sdk iphoneos Path").read().strip()

		ISDKP = os.popen("xcrun --sdk iphoneos --find clang").read().strip()
		ISDKP = os.path.dirname(ISDKP)+"/"

		ISDKF = "-arch armv7 -isysroot %s" % (ISDK)
		cmd = 'make HOST_CC="clang -m32 -arch i386" CROSS=%s TARGET_FLAGS="%s" TARGET_SYS=iOS' % (ISDKP, ISDKF)

	elif arch == "arm64":

		ISDK = os.popen("xcodebuild -version -sdk iphoneos Path").read().strip()

		ISDKP = os.popen("xcrun --sdk iphoneos --find clang").read().strip()
		ISDKP = os.path.dirname(ISDKP)+"/"
		

		ISDKF = "-arch arm64 -isysroot %s" % (ISDK)

		cmd = 'make CROSS=%s TARGET_FLAGS="%s" TARGET_SYS=iOS' % (ISDKP, ISDKF)

	elif arch == "i386":

		ISDK = os.popen("xcodebuild -version -sdk iphonesimulator Path").read().strip()

		ISDKP = os.popen("xcrun --sdk iphonesimulator --find clang").read().strip()
		ISDKP = os.path.dirname(ISDKP)+"/"
		

		ISDKF = "-arch i386 -isysroot %s" % (ISDK)

		cmd = 'make HOST_CC="clang -m32 -arch i386" CROSS=%s TARGET_FLAGS="%s" TARGET_SYS=iOS' % (ISDKP, ISDKF)
	else:
		ISDK = os.popen("xcodebuild -version -sdk iphonesimulator Path").read().strip()

		ISDKP = os.popen("xcrun --sdk iphonesimulator --find clang").read().strip()
		ISDKP = os.path.dirname(ISDKP)+"/"
		

		ISDKF = "-arch x86_64 -isysroot %s" % (ISDK)

		cmd = 'make HOST_CC="clang -m64 -arch x86_64" CROSS=%s TARGET_FLAGS="%s" TARGET_SYS=iOS' % (ISDKP, ISDKF)


	print("####SDKP")
	print(ISDKP)

	os.environ["ISDKF"] = ISDKF
	print("####SDKF")
	print(ISDKF)

	print("#####CMD")
	print(cmd)
	os.system(cmd)

	mv = "mv src/%s src/%s" % (LIBS[0], LIBS[0]+"."+arch)
	os.system(mv)



inp=""
for arch in ARCHS:
	inp += " src/%s.%s " % (LIBS[0], arch)
	
cmd = "xcrun lipo -create -output %s/%s %s" % (DEST, LIBS[0], inp)
print cmd
os.system(cmd)

'''
DEST= os.path.join(pwd, "output")
os.environ["DEST"] = DEST



#ARCHS= "i386 x86_64 armv7 arm64".split()
#ARCHS= "armv7 arm64".split()

ARCHS= "armv7".split()

#LIBS="libopencore-amrnb.a libopencore-amrwb.a".split()
LIBS="libluajit.a".split()

os.system("mkdir -p %s" % (DEST))

#os.system("./configure")


for arch in ARCHS:
	
	os.system("make clean")
	#os.system("rm CMakeCache.txt")
	#os.system("rm -rf CMakeFiles")

	IOSMV = "-miphoneos-version-min=7.0"
	os.environ["IOSMV"] = IOSMV
	oldPath = os.environ["PATH"]
	print "BuildArch", arch
	if arch == "armv7" or arch == "arm64":
		
		newPath = os.popen("xcodebuild -version -sdk iphoneos PlatformPath").read().strip()+"/Developer/usr/bin:"+oldPath
		os.environ["PATH"] = newPath
		print "####"
		print newPath

		SDK=os.popen("xcodebuild -version -sdk iphoneos Path").read().strip()
		os.environ["SDK"] = SDK
		os.environ["SDKROOT"] = SDK
		os.environ["sysroot"] = SDK
		print "####"
		print SDK

		CXX = "xcrun --sdk iphoneos clang++ -arch %s %s --sysroot=%s -isystem %s/usr/include" % (arch, IOSMV, SDK, SDK)
		os.environ["CXX"] = CXX
		print "####"
		print "CXX", CXX


		CC = "xcrun --sdk iphoneos clang -arch %s %s --sysroot=%s -isystem %s/usr/include" % (arch, IOSMV, SDK, SDK)
		os.environ["CC"] = CC
		os.environ["HOST_CC"] = CC
		
		print "####"
		print "CC", CC

		os.environ["ARCH"] = arch

		LDFLAGS="-Wl,-syslibroot,%s" % (SDK)
		os.environ["LDFLAGS"] = LDFLAGS
		print LDFLAGS

		os.environ["TARGET_SYS"] = arch

		#os.system("./configure --host=arm-apple-darwin --prefix=%s --disable-shared" % (DEST))
	else:

		newPath = os.popen("xcodebuild -version -sdk iphonesimulator PlatformPath").read().strip()+"/Developer/usr/bin:"+oldPath
		os.environ["PATH"] = newPath
		print newPath

		SDK=os.popen("xcodebuild -version -sdk iphonesimulator Path").read().strip()
		os.environ["SDK"] = SDK
		print SDK

		CXX = "xcrun --sdk iphoneos clang++ -arch %s %s --sysroot=%s -isystem %s/usr/include" % (arch, IOSMV, SDK, SDK)
		os.environ["CXX"] = CXX
		print CXX

		CC = "xcrun --sdk iphoneos clang++ -arch %s %s --sysroot=%s -isystem %s/usr/include" % (arch, IOSMV, SDK, SDK)
		os.environ["CC"] = CC
		print CC

		LDFLAGS="-Wl,-syslibroot,%s" % (SDK)
		os.environ["LDFLAGS"] = LDFLAGS
		print LDFLAGS

		#os.system("./configure --prefix=%s --disable-shared" % (DEST))

	#os.system("cmake -DCMAKE_BUILD_TYPE:STRING=Debug CMakeLists.txt")

	#os.system("make clean")
	os.system("make -j8")
	#os.system("make install")

	for i in LIBS:
		os.system("mv src/%s src/%s.%s" % (i, i, arch))
'''

'''
for i in LIBS:
	inp=""
	for arch in ARCHS:
		inp += " src/%s.%s " % (i, arch)
	
	cmd = "xcrun lipo -create -output %s/%s %s" % (DEST, i, inp)
	print cmd
	os.system(cmd)
'''

		

			
